<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'wordpress');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'wordpress');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '}xQ*V.IKP40~e;;:iT:,PpbUiE^%VF734[BB;d^H:,*NZK@8?oNje_E^C`VyB8s6');
define('SECURE_AUTH_KEY', 'GgZh}gr+W,UEwvO|,n+_Y!lD _yp2*Zm/]kI1+DEB!qA,%s*h,N 7_eSwyS6$+}S');
define('LOGGED_IN_KEY', '%1NdEp|@j (AF<&NJQZ`vOz3[BbxmMNzCeQEo+r]AXY{sMu{^5>3&j1BHB 2]a2e');
define('NONCE_KEY', 'm`nyKUa<F<HQjcBl50j{n<F,(S<S-Wg^wHLzPRHuPvn[gd(PDj]]vb&e]KXaQ.LS');
define('AUTH_SALT', '5^Ly)[hs<>@k6CTU;d8AYTK;qyrg8?T3-ylOu:0R9VlXE_2,+PQ@W&{}G5^Q_nl{');
define('SECURE_AUTH_SALT', '7DpBJ#&MiD3ork;7#,CU7SbOUm5tZ1MsNG(E?+Ov.M/5K{o#.MDsu7U5qie=SBCK');
define('LOGGED_IN_SALT', 'h.o^I`66h`wa1MC NrJXPEi8d!M64ZK`x}D#Hx7q+kg>yz4{r*Q>9sC`I Rq%rV_');
define('NONCE_SALT', ')_AgD4fDedeA}SOO-8YMPq62j<m}I7jsvBzP-t6k.X,n3V$,P*B.at4g}vfr^UHV');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'fj7A_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

