<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'wordpress');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'wordpress');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '; vK;g$x=rSaw;artfeRKhh*lRF|aa&eZ}Ot-}H-j!h&K$-8.y.yT?/u)d=}z~aI');
define('SECURE_AUTH_KEY', 'q2gp6~r&M<>X`Z-U!Cd El(P]hdheiCk.Okt3,3#9NL*1YE@o5J8Q[,y1>c=$,*=');
define('LOGGED_IN_KEY', '!wxJu}[Sc,sRkdfHV&vD8HoCDT.}ut3,22W}N-e7lLIE90y!69~iWkZ~{gWKZ:9(');
define('NONCE_KEY', '6oKhZ@%?e[k{Ge-Z/PRsMSHx8Je<&+;S7A(,C_p._(m=ph7KA@A<<5nBCFd5BDTt');
define('AUTH_SALT', 'Fz@zd]O8]=881?h^QDu!@Bf3`0M6Wxc`wwfMVwCH@Opf|J8kVP.6kCg*fw)J|*/>');
define('SECURE_AUTH_SALT', 'gKS2g5zH0+7md{=8}E@|nN20bwU6{QjU5fml-qP=FrQj6)3WZhuWM5]d{42pRL;%');
define('LOGGED_IN_SALT', 'yun5DE8<>V[pGM^,_B:wHC)[}@|&D-SP9_Oc6hLTQ&O:ZJl{HG@6Vz2I[AT$VhO!');
define('NONCE_SALT', 'wX*y-orL~q/w 4lS%-bOj:]qR#u~n_%kMXC+][nQnz3?ZzSf|nQ2B5M42E5Ud4aJ');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wpjf_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

